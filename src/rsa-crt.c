#include <bigi.h>
#include <stddef.h>
#include <rsa-crt.h>

#include <bigi_io.h>

#ifndef __MIKROC_PRO_FOR_ARM__
    #include <stdlib.h>
    #include <stdio.h>
    #include <string.h>
#endif /* #ifndef __MIKROC_PRO_FOR_ARM__ */
#ifdef HW_RSA
    #include "__rsa_defines.h"

    #define CYPHER_4096bit_BYTES 512
    #define CYPHER_4096bit_WORDS 128
    BUFF8 p_buff8_array;
    BUFF8 q_buff8_array;
    BUFF8 e_buff8_array;
    BUFF8 d_buff8_array;
    BUFF8 dp_buff8_array;
    BUFF8 dq_buff8_array;
    BUFF8 qi_buff8_array;
    BUFF8 n_buff8_array;
    BUFF8 m_buff8_array;
    BUFF8 c_buff8_array;

    byte_t test_array[CYPHER_4096bit_BYTES];

    byte_t p_io_byte_ary[CYPHER_4096bit_BYTES];
    byte_t q_io_byte_ary[CYPHER_4096bit_BYTES];
    byte_t e_io_byte_ary[CYPHER_4096bit_BYTES];
    byte_t d_io_byte_ary[CYPHER_4096bit_BYTES];
    byte_t dp_io_byte_ary[CYPHER_4096bit_BYTES];
    byte_t dq_io_byte_ary[CYPHER_4096bit_BYTES];
    byte_t qi_io_byte_ary[CYPHER_4096bit_BYTES];
    byte_t n_io_byte_ary[CYPHER_4096bit_BYTES];
    byte_t m_io_byte_ary[CYPHER_4096bit_BYTES];
    byte_t c_io_byte_ary[CYPHER_4096bit_BYTES];

    volatile unsigned int  bytes_read = 0;
    uint8_t rsa_status = 0;
#endif

/** ----------------------------------------------------------------------------
 *  Initialize Paillier cryptosystem
 *                                                                            */
BIGI_STATUS rsa_crt_init(const bigi_t *const p,
                         const bigi_t *const q,
                         const bigi_t *const e,
                         bigi_t *const TMPARY,
                         const index_t tmparylen,
                         bigi_t *const n,
                         bigi_t *const d,
                         bigi_t *const dp,
                         bigi_t *const dq,
                         bigi_t *const qi)
{

    cmp_t cmp = CMP_UNDEFINED;
    bigi_t * tmp_result_0 = NULL;
    bigi_t * tmp_result_1 = NULL;
    bigi_t * tmp_result_2 = NULL;

    /* check if P == 0 */
    BIGI_CALL(bigi_is_zero(p, &cmp))
    if (cmp == CMP_EQUAL)
        return ERR_INVALID_PARAMS;

    /*check if Q == 0 */
    BIGI_CALL(bigi_is_zero(q, &cmp))
    if (cmp == CMP_EQUAL)
        return ERR_INVALID_PARAMS;

    /*check if E == 0 */
    BIGI_CALL(bigi_is_zero(e, &cmp))
    if (cmp == CMP_EQUAL)
        return ERR_INVALID_PARAMS;

    tmp_result_0  = &TMPARY[0];
    tmp_result_1  = &TMPARY[1];
    tmp_result_2  = &TMPARY[2];

    /*calculate and save p -1 and q - 1 */
    bigi_sub_one(p, tmp_result_0);
    bigi_sub_one(q, tmp_result_1); 

    /*calculate and save n */
    bigi_mult_fit(p, q, &TMPARY[3], tmparylen - 3, n);

    /*calculate and save Phi*/
    bigi_mult_fit(tmp_result_0, tmp_result_1, &TMPARY[3], tmparylen - 3, tmp_result_2);

    /*calculate and save D*/
    bigi_mod_inv(e, tmp_result_2, &TMPARY[3], tmparylen - 3, d);

    /*calculate and save dp*/
    /*temporarly se wordlen to match D lenght*/
    dp->wordlen = d->wordlen;
    bigi_mod_red(d, tmp_result_0, &TMPARY[3], tmparylen - 3, dp);
    dp->wordlen = (d->wordlen) >> 1;
    
    /*calculateand save dq*/
    /*temporarly se wordlen to match D lenght*/
    dq->wordlen = d->wordlen;
    bigi_mod_red(d, tmp_result_1, &TMPARY[3], tmparylen - 3, dq);
    dq->wordlen = (d->wordlen) >> 1;

    /*calculate and save qi*/
    bigi_mod_inv_binary(q, p, &TMPARY[3], tmparylen - 3, qi);
    return OK;
}

#ifdef HW_RSA
BIGI_STATUS rsa_crt_hw_init(const bigi_t *const p,
                            const bigi_t *const q,
                            const bigi_t *const e,
                            const bigi_t *const n,
                            const bigi_t *const d,
                            const bigi_t *const dp,
                            const bigi_t *const dq,
                            const bigi_t *const qi)
{
    p_buff8_array.len =  (d->wordlen * MACHINE_WORD_BITLEN) >> 4;
    q_buff8_array.len =  (d->wordlen * MACHINE_WORD_BITLEN) >> 4;
    e_buff8_array.len =  (d->wordlen * MACHINE_WORD_BITLEN) >> 3;
    d_buff8_array.len =  (d->wordlen * MACHINE_WORD_BITLEN) >> 3;
    dp_buff8_array.len = (d->wordlen * MACHINE_WORD_BITLEN) >> 4;
    dq_buff8_array.len = (d->wordlen * MACHINE_WORD_BITLEN) >> 4;
    qi_buff8_array.len = (d->wordlen * MACHINE_WORD_BITLEN) >> 4;
    n_buff8_array.len =  (d->wordlen * MACHINE_WORD_BITLEN) >> 3;
    m_buff8_array.len =  (d->wordlen * MACHINE_WORD_BITLEN) >> 3;
    c_buff8_array.len =  (d->wordlen * MACHINE_WORD_BITLEN) >> 3;

    p_buff8_array.pd =  (char*)&p_io_byte_ary[0];
    q_buff8_array.pd =  (char*)&q_io_byte_ary[0];
    e_buff8_array.pd =  (char*)&e_io_byte_ary[0];
    d_buff8_array.pd =  (char*)&d_io_byte_ary[0];
    dp_buff8_array.pd = (char*)&dp_io_byte_ary[0];
    dq_buff8_array.pd = (char*)&dq_io_byte_ary[0];
    qi_buff8_array.pd = (char*)&qi_io_byte_ary[0];
    n_buff8_array.pd =  (char*)&n_io_byte_ary[0];
    m_buff8_array.pd =  (char*)&m_io_byte_ary[0];
    c_buff8_array.pd =  (char*)&c_io_byte_ary[0];

    bigi_to_bytes(p_buff8_array.pd,  (d->wordlen * MACHINE_WORD_BITLEN) >> 4, p);
    bigi_to_bytes(q_buff8_array.pd,  (d->wordlen * MACHINE_WORD_BITLEN) >> 4, q);
    bigi_to_bytes(e_buff8_array.pd,  (d->wordlen * MACHINE_WORD_BITLEN) >> 3, e);
    bigi_to_bytes(d_buff8_array.pd,  (d->wordlen * MACHINE_WORD_BITLEN) >> 3, d);
    bigi_to_bytes(dp_buff8_array.pd, (d->wordlen * MACHINE_WORD_BITLEN) >> 4, dp);
    bigi_to_bytes(dq_buff8_array.pd, (d->wordlen * MACHINE_WORD_BITLEN) >> 4, dq);
    bigi_to_bytes(qi_buff8_array.pd, (d->wordlen * MACHINE_WORD_BITLEN) >> 4, qi);
    bigi_to_bytes(n_buff8_array.pd,  (d->wordlen * MACHINE_WORD_BITLEN) >> 3, n);

    rsa_status = PKE_RET_OK;
    pke_power(0x01);
    while(pke_busy() == 0x01) asm nop;
    
    pke_reset();
    while(pke_busy() == 0x01) asm nop;

    rsa_status = rsa_load_key((d->wordlen * MACHINE_WORD_BITLEN), &d_buff8_array, &n_buff8_array, &e_buff8_array, BIG_ENDIAN);
    if (rsa_status != PKE_RET_OK)
        return ERR_UNDEFINED;
    while(pke_busy() == 1) asm nop;

    rsa_status = rsa_crt_gen_params((d->wordlen * MACHINE_WORD_BITLEN), &p_buff8_array, &q_buff8_array, &n_buff8_array, &d_buff8_array, BIG_ENDIAN);
    if (rsa_status != PKE_RET_OK)
        return ERR_UNDEFINED;
    while(pke_busy() == 1) asm nop;

    rsa_status = rsa_load_crt_params((d->wordlen * MACHINE_WORD_BITLEN), &dp_buff8_array, &dq_buff8_array, &qi_buff8_array, BIG_ENDIAN);
    if (rsa_status != PKE_RET_OK) 
        return ERR_UNDEFINED;
   while(pke_busy() == 1) asm nop;
}
#endif
/** ----------------------------------------------------------------------------
 *  Encryption
 *                                                                            */
BIGI_STATUS rsa_encr(const bigi_t *const m,
                     const bigi_t *const e,
                     const bigi_t *const n,
                     bigi_t *const TMPARY,
                     const index_t tmparylen,
                     bigi_t *const c)
{
    cmp_t cmp = CMP_UNDEFINED;
    word_t mu_mult;
    bigi_t * tmp_result_0 = NULL;
    
    /*check if M == 0*/
    BIGI_CALL(bigi_is_zero(m, &cmp))
    if (cmp == CMP_EQUAL)
        return ERR_INVALID_PARAMS;

    /*check if E == 0*/
    BIGI_CALL(bigi_is_zero(e, &cmp))
    if (cmp == CMP_EQUAL)
        return ERR_INVALID_PARAMS;

    /*check if N == 0*/
    BIGI_CALL(bigi_is_zero(n, &cmp))
    if (cmp == CMP_EQUAL)
        return ERR_INVALID_PARAMS;
    
    tmp_result_0  = &TMPARY[0];

    bigi_get_mont_params(n, &TMPARY[1], tmparylen - 1, tmp_result_0, &mu_mult);
    bigi_mod_exp_mont(m, e, n, tmp_result_0, mu_mult, &TMPARY[1], tmparylen - 1, c);

    return OK;
}

#ifdef HW_RSA
BIGI_STATUS rsa_hw_encr(const bigi_t *const m, bigi_t *const c)
{
    rsa_status =  PKE_RET_OK;
    bigi_to_bytes(m_buff8_array.pd, (m->wordlen * MACHINE_WORD_BITLEN) >> 3, m);

    rsa_status = rsa_encrypt((m->wordlen * MACHINE_WORD_BITLEN), &m_buff8_array, BIG_ENDIAN);
    pke_start(0);
    while(pke_busy() == 1) asm nop;
    if (rsa_status != PKE_RET_OK)
        return ERR_UNDEFINED;
    
    bytes_read = pke_read_scm(c_buff8_array.pd, (c->wordlen * MACHINE_WORD_BITLEN) >> 3, 5, BIG_ENDIAN);
    if (bytes_read != ((c->wordlen * MACHINE_WORD_BITLEN) >> 3))
        return ERR_UNDEFINED;

    bigi_from_bytes(c, c_buff8_array.pd, (c->wordlen * MACHINE_WORD_BITLEN) >> 3);
}
#endif
/** ----------------------------------------------------------------------------
 *  Decryption
 *                                                                            */
BIGI_STATUS rsa_crt_decr(const bigi_t *const c,
                         const bigi_t *const p,
                         const bigi_t *const q,
                         const bigi_t *const dp,
                         const bigi_t *const dq,
                         const bigi_t *const qi,
                         const bigi_t *const n,
                         bigi_t *const TMPARY,
                         const index_t tmparylen,
                         bigi_t *const m)
{
    word_t mu_mult;
    word_t index_value;
    bigi_t * tmp_result_0 = NULL;
    bigi_t * tmp_result_1 = NULL;
    bigi_t * tmp_result_2 = NULL;
    bigi_t * tmp_result_3 = NULL;
    bigi_t * p_copy = NULL;
    bigi_t * q_copy = NULL;

    tmp_result_0  = &TMPARY[0];
    tmp_result_1  = &TMPARY[1];
    tmp_result_2  = &TMPARY[2];
    tmp_result_3  = &TMPARY[3];
    p_copy        = &TMPARY[4];
    q_copy        = &TMPARY[5];
    bigi_set_zero(p_copy);
    bigi_set_zero(q_copy);
    bigi_copy(p_copy, p);
    bigi_copy(q_copy, q);

    /*tmp0 = C mod P*/
    /*temporarly se wordlen to match C lenght*/
    p_copy->wordlen = c->wordlen;
    bigi_mod_red(c, p_copy, &TMPARY[6], tmparylen - 6, tmp_result_0);

    p_copy->wordlen = c->wordlen >> 1;
    tmp_result_0->wordlen = c->wordlen >> 1;
    tmp_result_1->wordlen = c->wordlen >> 1;
    tmp_result_2->wordlen = c->wordlen >> 1;
    p_copy->domain       = DOMAIN_NORMAL;
    tmp_result_1->domain = DOMAIN_NORMAL;

    /*m1 = tmp0^Dp mod P in Mont. Domain*/
    bigi_get_mont_params(p_copy, &TMPARY[6], tmparylen - 6, tmp_result_1, &mu_mult);
    bigi_mod_exp_mont(tmp_result_0, dp, p_copy, tmp_result_1, mu_mult, &TMPARY[6], tmparylen - 6, tmp_result_2);

    /*tmp0 = C mod Q*/
    /*temporarly se wordlen to match C lenght*/
    q_copy->wordlen = c->wordlen;
    tmp_result_0->wordlen = c->wordlen;
    bigi_mod_red(c, q_copy, &TMPARY[6], tmparylen - 6, tmp_result_0);

    q_copy->wordlen = c->wordlen >> 1;
    tmp_result_0->wordlen = c->wordlen >> 1;
    tmp_result_1->wordlen = c->wordlen >> 1;
    bigi_set_zero(tmp_result_3);
    tmp_result_3->wordlen = c->wordlen >> 1;
    q_copy->domain       = DOMAIN_NORMAL;
    tmp_result_1->domain = DOMAIN_NORMAL;

    /*m2 = tmp0^Dq mod Q in Mont. Domain*/
    bigi_get_mont_params(q_copy, &TMPARY[6], tmparylen - 6, tmp_result_1, &mu_mult);
    bigi_mod_exp_mont(tmp_result_0, dq, q_copy, tmp_result_1, mu_mult, &TMPARY[6], tmparylen - 6, tmp_result_3);

    /*h  = (m1 - m2) * qi mod P*/
        /* tmp0 = m1 - m2*/
    bigi_sub(tmp_result_2, tmp_result_3, tmp_result_0);

    /* Check if result is negative number, then add P */
    index_value = 0;
    bigi_get_bit(tmp_result_0, ((dp->wordlen * MACHINE_WORD_BITLEN) + 1), &index_value);
    if (index_value == 1)
        bigi_add(tmp_result_0, p, tmp_result_1);
    else
        bigi_copy(tmp_result_1, tmp_result_0);

    bigi_set_zero(q_copy);
    bigi_copy(q_copy, qi);
    q_copy->wordlen = c->wordlen >> 1;

    /* tmp0 = (m1- m2) * qi mod P*/
    bigi_mod_mult(tmp_result_1, q_copy, p_copy, tmp_result_0);

    bigi_set_zero(q_copy);
    bigi_copy(q_copy, q);
    q_copy->wordlen = c->wordlen;
    tmp_result_0->wordlen = c->wordlen;
    tmp_result_1->wordlen = c->wordlen;
    
    tmp_result_3->wordlen = c->wordlen;

    /* m = m2 + h * Q*/
        /* tmp1 = h * Q*/
    bigi_mult_fit(tmp_result_0, q_copy, &TMPARY[6], tmparylen - 6, tmp_result_1);
        /* m2 + tmp1*/
    bigi_add(tmp_result_3, tmp_result_1, m);
    return OK;
}
#ifdef HW_RSA
BIGI_STATUS rsa_crt_hw_decr(const bigi_t *const c, bigi_t *const m)
{
    rsa_status =  PKE_RET_OK;
    bigi_to_bytes(c_buff8_array.pd, (c->wordlen * MACHINE_WORD_BITLEN) >> 3, c);

    rsa_status = rsa_crt_decrypt((c->wordlen * MACHINE_WORD_BITLEN), &c_buff8_array, BIG_ENDIAN);
    pke_start(0);
    
    if (rsa_status != PKE_RET_OK)
        return ERR_UNDEFINED;
    
    while(pke_busy() == 1) asm nop;

    bytes_read = pke_read_scm(m_buff8_array.pd, (m->wordlen * MACHINE_WORD_BITLEN) >> 3, 5, BIG_ENDIAN);
    if (bytes_read != ((m->wordlen * MACHINE_WORD_BITLEN) >> 3))
        return ERR_UNDEFINED;

    bigi_from_bytes(m, m_buff8_array.pd, (m->wordlen * MACHINE_WORD_BITLEN) >> 3);
}
#endif