#!/bin/bash

reset
cd ../../bigi/
    ./build.sh -c
    cd ../rsa-crt/
./build.sh -c

echo ""
echo ""
echo "    Running bigi examples"
echo "================================================================================"
echo ""

../bigi/bin/desktop_example | ruby

echo ""
echo ""
echo "    Running RSA-CRT examples"
echo "================================================================================"
echo ""

./bin/desktop_example | ruby

echo ""
