#ifndef RSA_CRT_H
#define RSA_CRT_H

#ifdef __cplusplus
extern "C" {
#endif

#include <bigi.h>

#define HW_RSA

#ifndef __MIKROC_PRO_FOR_ARM__
    #undef HW_RSA
#endif

#ifdef HW_RSA
    #define RSA_SLOT_MOD_P2 0
    #define RSA_SLOT_MOD_Q2 1
    #define LITTLE_ENDIAN   0
    #define BIG_ENDIAN      1

#endif /*HW_RSA*/
/**
 *  @brief          Initialize with p,q such that gcd(pq, (p-1)(q-1) = 1) - this holds if both are the same length
 *
 *  @param[in]      Large prime
 *  @param[in]      Large prime
 *  @param[out]     Private key
 *  @param[out]     Public key
 *
 */
BIGI_STATUS rsa_crt_init(const bigi_t *const p,
                         const bigi_t *const q,
                         const bigi_t *const e,
                         bigi_t *const TMPARY,
                         const index_t tmparylen,
                         bigi_t *const n,
                         bigi_t *const d,
                         bigi_t *const dp,
                         bigi_t *const dq,
                         bigi_t *const qi);
#ifdef HW_RSA
BIGI_STATUS rsa_crt_hw_init(const bigi_t *const p,
                            const bigi_t *const q,
                            const bigi_t *const e,
                            const bigi_t *const n,
                            const bigi_t *const d,
                            const bigi_t *const dp,
                            const bigi_t *const dq,
                            const bigi_t *const qi);
#endif

/**
 *  @brief          Encrypt message m < n
 *
 *  @param[in]      Public key
 *  @param[in]      Plaintext
 *  @param[out]     Resulting ciphertext, valid only if the return value is 0
 *  @return         0 if OK, 1 if not m < n
 *
 */
BIGI_STATUS rsa_encr(const bigi_t *const m,
                     const bigi_t *const e,
                     const bigi_t *const n,
                     bigi_t *const TMPARY,
                     const index_t tmparylen,
                     bigi_t *const c);
#ifdef HW_RSA
BIGI_STATUS rsa_hw_encr(const bigi_t *const m, bigi_t *const c);
#endif

/**
 *  @brief          Decrypt ciphertext
 *
 *  @param[in]      Public key
 *  @param[in]      Private key
 *  @param[in]      ciphertext
 *  @param[out]     Resulting plaintext, valid only if the return value is 0
 *  @return         0 if OK, 1 if not m < n
 *
 */
BIGI_STATUS rsa_crt_decr(const bigi_t *const c,
                         const bigi_t *const p,
                         const bigi_t *const q,
                         const bigi_t *const dp,
                         const bigi_t *const dq,
                         const bigi_t *const qi,
                         const bigi_t *const n,
                         bigi_t *const TMPARY,
                         const index_t tmparylen,
                         bigi_t *const m);
#ifdef HW_RSA
BIGI_STATUS rsa_crt_hw_decr(const bigi_t *const c, bigi_t *const m);
#endif

#ifdef __cplusplus
}
#endif /* #ifdef __cplusplus */

#endif /* #ifndef RSA_CRT_H */